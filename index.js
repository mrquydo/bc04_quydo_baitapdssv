const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";

//  chức năng thêm sinh viên
var dssv = [];
// lấy thông tin từ localStorage

var dssvJson = localStorage.getItem(DSSV_LOCALSTORAGE);
if (dssvJson != null) {
  dssv = JSON.parse(dssvJson);
  //  array khi convert thành json sẽ mất function, ta sẽ map lại
  for (var index = 0; index < dssv.length; index++) {
    var sv = dssv[index];
    dssv[index] = new SinhVien(
      sv.ten,
      sv.ma,
      sv.matKhau,
      sv.email,
      sv.toan,
      sv.ly,
      sv.hoa
    );
  }

  renderDSSV(dssv);
}
function themSV() {
  var newSv = layThongTinTuForm();

  var isValid =
    validation.kiemTraRong(newSv.ma, "spanMaSV", "Ma sv khong duoc de rong") &&
    validation.kiemTraDoDai(newSv.ma, "spanMaSV", "Ma sv gom 4 ky tu", 4, 4);

  isValid =
    isValid &
    validation.kiemTraRong(newSv.ten, "spanTenSV", "Ten sv khong duoc de rong");

  isValid =
    isValid &
      validation.kiemTraRong(
        newSv.email,
        "spanEmailSV",
        "Email sv khong duoc de rong"
      ) &&
    validation.kiemTraEmail(newSv.email, "spanEmailSV", "Email khong dung");

  isValid =
    isValid &
    validation.kiemTraRong(
      newSv.matKhau,
      "spanMatKhau",
      "MK sv khong duoc de rong"
    ) &
    validation.kiemTraRong(newSv.toan, "spanToan", "Toan khong duoc de rong") &
    validation.kiemTraRong(newSv.ly, "spanLy", "Ly khong duoc de rong") &
    validation.kiemTraRong(newSv.hoa, "spanHoa", "Hoa khong duoc de rong");

  if (isValid) {
    dssv.push(newSv);

    // tạo json
    var dssvJson = JSON.stringify(dssv);
    // console.log("dssvJson: ", dssvJson);
    // lưu json vào localStorage
    localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
    renderDSSV(dssv);
    1;
  }
}

function xoaSinhVien(id) {
  console.log(id);

  // var index = dssv.findIndex(function (sv) {
  //   return sv.ma == id;
  // });
  var index = timKiemViTri(id, dssv);
  // tìm thấy vị trí
  if (index != -1) {
    dssv.splice(index, 1);
    renderDSSV(dssv);
  }
}

function suaSinhVien(id) {
  var index = timKiemViTri(id, dssv);
  console.log("index: ", index);
  if (index != -1) {
    var sv = dssv[index];
    document.getElementById("txtMaSV").setAttribute("disabled", "");
    showThongTinLenForm(sv);
  }
}

function capNhatSinhVien() {
  var id = document.getElementById("txtMaSV").value;
  var index = timKiemViTri(id, dssv);
  var sv = dssv[index];
  sv.ma = document.getElementById("txtMaSV").value;
  sv.ten = document.getElementById("txtTenSV").value;
  sv.email = document.getElementById("txtEmail").value;
  sv.matKhau = document.getElementById("txtPass").value;
  sv.toan = document.getElementById("txtDiemToan").value;
  sv.ly = document.getElementById("txtDiemLy").value;
  sv.hoa = document.getElementById("txtDiemHoa").value;
  renderDSSV(dssv);
}

function reSetSinhVien() {
  document.getElementById("txtMaSV").value = "";
  document.getElementById("txtTenSV").value = "";
  document.getElementById("txtEmail").value = "";
  document.getElementById("txtPass").value = "";
  document.getElementById("txtDiemToan").value = "";
  document.getElementById("txtDiemLy").value = "";
  document.getElementById("txtDiemHoa").value = "";
  document.getElementById("txtMaSV").removeAttribute("disabled");
}

function searchSinhVien() {
  var name = document.getElementById("txtSearch").value;
  var index = timKiemViTriTheoTen(name, dssv);
  var sv = dssv[index];
  var newDSSV = [];
  newDSSV.push(sv);
  renderDSSV(newDSSV);
}
